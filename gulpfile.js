'use strict';
const gulp 			= require('gulp');
const $ 			= require('gulp-load-plugins')();
const bs 			= require('browser-sync').create();

const optionsServe 	= {
	src: ['build']
}

let HubRegistry = require('gulp-hub');

// load some files into the registry
let hub = new HubRegistry(['tasks/*.js']);

gulp.registry(hub);

gulp.task('fonts', function() {
	return gulp.src('dev/common/fonts/*')
		.pipe( gulp.dest( 'build/fonts' ) );
});

gulp.task('watch:sprite', function() {
	gulp.watch('dev/common/img/sprite/*', gulp.series('sprite'))
});

gulp.task('watch:img', function() {
	gulp.watch('dev/common/img/*.*', gulp.series('img'))
});

gulp.task( 'serve', function(){
	bs.init({
		server: optionsServe.src
	});
	bs.watch('dev/**/*.styl').on('change', gulp.series('stylus', bs.reload) )
	bs.watch('dev/**/**/*.pug').on('change', gulp.series('pug', bs.reload) )
});





gulp.task('default', gulp.series(
		'clean',
		'fonts',
		'stylus',
		'pug',
		'webpack',
		gulp.parallel('sprite', 'img', 'watch:sprite', 'watch:img', 'serve' )
	)
);