'use strict';

function mHeight(){
	var maxH = 0
	if($('.matchHeight').length > 0){
		$('.matchHeight').each(function(){
			var curH = $(this).height()
			if(maxH < curH){
				maxH = curH
			}
		});

		$('.matchHeight').css({'min-height': maxH})
	}
}

function hideBtnMore(idComponent){

	$('.show_more').each(function(){
		var ident = $(this).data('ident');
		if(ident !== undefined && ident == idComponent){
			$(this).addClass('inv');
		}
	});

}

function postsMasonryInit(parentClass, postClass){

	var msnry = new Masonry( '.'+parentClass, {
		itemSelector: '.'+postClass,
		columnWidth: '.'+postClass,
		gutter: 20
	});

}


$(document).ready(function(){
	imagesLoaded('.main_wrap', function() {
		mHeight()
	});

	$('.header__burger-btn').click(function(){
		var header = $('.header__menu.header')
		var btn = $(this)
		var isActive = header.hasClass('active')

		if(isActive){
			btn.removeClass('active')
			header.removeClass('active').fadeOut();
		} else {
			btn.addClass('active')
			header.addClass('active').fadeIn();
		}
	});

	$('.collection__link').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		var descH = $('.collection__desc').outerHeight(true)
		var objThis = $(this);
		var openText = objThis.data('open');
		var hideText = objThis.data('hide');

		if(objThis.hasClass('open')){
			objThis.removeClass('open').html(openText);
			$('.collection__desc').css({'padding-bottom': '6rem'})
			$('.hidden__text').slideUp(function(){

			});
		} else {
			objThis.addClass('open').html(hideText);
			$('.collection__desc').css({'padding-bottom': '10rem'})
			$('.hidden__text').slideDown(function(){

			});
		}
	});

	$('.cp__more').click(function(e){
		var objThis = $(this);
		if(objThis.hasClass('open')){
			objThis.removeClass('open');
			objThis.next().slideUp();
		} else {
			objThis.addClass('open');
			objThis.next().slideDown();
		}
	});

	$('.search__select').select2({
		minimumResultsForSearch: Infinity,
		width: '99%',
		placeholder: function(){
			$(this).data('placeholder');
		}
	});

	$('.card__btn').click(function(){
		window.location = 'http://pic.wellwedo.com/templates/1944/build/card-page/card-page.html'
	});

	$('.modal__close').click(function(){
		$('.card__modal').fadeOut();
	});

	var items = document.querySelector('.posts__container .post')
	if(items){
		var cloned = items.cloneNode(true);
	}

	if($('.c_list').length >0){

		if($('.posts__container').length >0){
			postsMasonryInit('posts__container', 'post');
		}

		//объявляем глобальную переменную
		window.curpage = {};
		window.curpage.articles = 2;
		window.curpage.catalog = 2;

		//открытие аякс статей
		if($('.show_more').length){
			var newHtml = '';

			$("body").on('click', '.show_more', function(){
				var idComponent = $(this).data('ident');
				var url = $(this).data('url');
				var langFilter = $(this).data('langFilter');

				if(idComponent){
					var cntPages = $('.cnt_pages_'+idComponent).data('cnt');
					var pathToController = '/local/include/'+idComponent+'.php';

		    		//подхватываем значения фильтра
		    		var dataFilter = '';
		    		if(idComponent == 'catalog'){
		    			dataFilter = $('#picture_filter').serialize();
		    		}

			        $.ajax({
						type: "GET",
						url: pathToController,
						data: "isAjax=Y&PAGEN_1="+window.curpage[idComponent]+"&url="+url+"&langFilter="+langFilter+'&'+dataFilter,
						dataType: 'html',
						success: function(data){
							if (data) {
								var objParent = $(data).find('.c_list');
								if (objParent.length){
									var newHtml = objParent.html();
									if(newHtml){
										$(".c_list_"+idComponent).append(newHtml);

										if(idComponent == 'articles'){
											postsMasonryInit('posts__container', 'post');
										}

										window.curpage[idComponent]++;

										if(window.curpage[idComponent] > cntPages){
											hideBtnMore(idComponent);
										}
									}

								}
							}
						}
					});
				}

				return false;
			});
		}
	}

});