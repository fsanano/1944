'use strict';
const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');
const postcss 		= require('gulp-postcss');
const lost 			= require('lost');
const rupture 		= require('rupture');


const optionStylus 	= {
	src: ['dev/common/stylus/common.styl'],
	// src: ['dev/pages/**/*.styl', 'dev/common/stylus/common.styl'],
	dist: 'build'
}


gulp.task('stylus', function(){
	return gulp.src( optionStylus.src )
		.pipe( $.plumber() )
		// .pipe( $.sourcemaps.init()  )
		.pipe( $.stylus({
			// compress: true,
			use: [ rupture() ]
		}) )
		.pipe( postcss([
			lost()
		]) )
		.pipe( $.autoprefixer('>0%') )
		// .pipe( $.sourcemaps.write() )
		.pipe( gulp.dest( optionStylus.dist ) );
});