"use strict";

const $ 				= require('gulp-load-plugins')();
const gulp 				= require('gulp');
const usemin 			= require('gulp-jade-usemin');
const optionsUsemin 	= {
	src: 'dev/views/*.jade'
};

gulp.task( 'usemin', function(){
	gulp.src( optionsUsemin.src )
		.pipe(usemin({
			css: [ $.rev() ],
			js: [ $.uglify, $.rev() ],
			inlinejs: [ $.uglify ]
		}))
		.pipe(gulp.dest( optionsUsemin.dist + branchName ));
});