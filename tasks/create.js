const gulp 			= require('gulp');
const fs 			= require('fs');
const argv 			= require('yargs').argv;
const $ 			= require('gulp-load-plugins')();

const pageDir = 'dev/pages/' + argv.name
const pagePath = 'dev/pages/' + argv.name + '/' + argv.name

const componentDir = 'dev/components/' + argv.name
const componentPath = 'dev/components/' + argv.name + '/_' + argv.name


var pugcontent = '';
var stylcontent = '';


const create = function(dir, path, callback, pug, styl){

	fs.stat(dir, function(err, stat) {
		if(err == null) {
			console.log( dir + ' directory exists');
			createJs(dir, path, callback);
		} else {
			console.log('.styl and .pug files created in ' + dir);
			fs.mkdir(dir, callback);
			fs.writeFile( path +'.styl' , stylcontent , callback);
			fs.writeFile( path +'.pug' , pug , callback);
			createJs(dir, path, callback);
		}
	});

	callback();
}


const createJs = function(dir, path){
	if( argv.js ){
		fs.stat(path + '.js', function(err, stat) {
			if(err == null) {
				console.log('js file exist')
			} else {
				fs.writeFile( path +'.js' , "'use strict';\n");
				console.log('.js file created')
			}
		});
	}
}


gulp.task('page', function(cb){

	pugcontent = 'extends ../../common/pug/base/base \n\nblock content\n\t'
	stylcontent = "@import '../../common/stylus/page.styl'\n\n// below here include components styles\n"

	create(pageDir, pagePath, cb, pugcontent, stylcontent)

});

gulp.task('component', function(cb){

	pugcontent = '.' + argv.name + 'block\n'
	stylcontent = '.' + argv.name + '\n\t//'

	create(componentDir, componentPath, cb, pugcontent, stylcontent)

});