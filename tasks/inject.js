'use strict';
const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');

const optionsInject 	= {
	target: 'build/**/*.html',
	sources: [ 'build/**/*.js', 'build/**/*.css' ],
	dist: 'build'
}

gulp.task('inject', function(){

	let targes = gulp.src( optionsInject.target);

	let sources = gulp.src( optionsInject.sources, {read: false})

	return targes.pipe( $.inject( sources , { relative: true, addRootSlash: false, ignorePath: ['../common.js', '../common.css']} ) )
		.pipe( gulp.dest( optionsInject.dist ) );

});